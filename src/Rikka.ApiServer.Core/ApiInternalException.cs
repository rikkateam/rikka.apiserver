using System;

namespace Rikka.ApiServer.Core
{
    public class ApiInternalException : Exception
    {
        public string Code { get; }

        public ApiInternalException(string code, string message):base(message)
        {
            Code = code;
            //TODO: ����� �������� � ������ ����� ����
        }
    }
}