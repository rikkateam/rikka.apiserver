using System;

namespace Rikka.ApiServer.Core.Attributes
{
    /// <summary>
    /// ��������� ����� ��� ������� ������������ ��� ������ ������� ����,
    /// ��������� ��� ������ � ������������ � ������������ �� ��������
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ApiTypeMapAttribute : Attribute
    {
        public ApiTypeMapAttribute(Type fromType, Type toType)
        {
            FromType = fromType;
            ToType = toType;
        }
        public Type FromType { get; set; }
        public Type ToType { get; set; }
    }
}