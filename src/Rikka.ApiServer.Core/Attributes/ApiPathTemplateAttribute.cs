using System;

namespace Rikka.ApiServer.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ApiPathTemplateAttribute : Attribute
    {
        public ApiPathTemplateAttribute(string template)
        {
            Template = template;
        }

        public string Template { get; set; }
    }
}