using System;

namespace Rikka.ApiServer.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ApiHttpPutAttribute : Attribute
    {

    }
}