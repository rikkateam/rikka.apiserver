using System;

namespace Rikka.ApiServer.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ApiContextMarkAttribute : Attribute
    {
        public ApiContextMarkAttribute(string version)
        {
            Version = ApiVersion.FromString(version);
        }
        public ApiVersion Version { get; set; }
    }
}