using System;

namespace Rikka.ApiServer.Core.Attributes
{
    /// <summary>
    /// ��������� ��� ����� API �� ������ ��������� ��������
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ApiAnonymousAttribute : Attribute
    {

    }
}