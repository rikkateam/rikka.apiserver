﻿using System.Threading.Tasks;

namespace Rikka.ApiServer.Core.Telementry
{
    public interface IServerTelemetry
    {
        void AddHit(string category, string name);
        void AddTimer(string category, string name, int miliseconds);
        Task Store();
    }
}