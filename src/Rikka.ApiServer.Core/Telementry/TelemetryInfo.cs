using System;
using System.Diagnostics;

namespace Rikka.ApiServer.Core.Telementry
{
    public class TelemetryInfo
    {
        public TelemetryInfo(string type,string category, string name, string value, string additional=null)
        {
            Type = type;
            Category = category;
            Name = name;
            Value= value;
            At = DateTime.UtcNow;
            Additional = additional;

#if (DEBUG)
            var message = $"{At.ToLocalTime()} [{category}] {name}: value {Value}";
            Console.WriteLine(message);
            Debug.WriteLine(message);
#endif
        }
        public string Type { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Additional { get; set; }
        public DateTime At { get; set; }
    }
}