using System;

namespace Rikka.ApiServer.Core.Telementry
{
    public class TelemetryTimer : IDisposable
    {
        private readonly IServerTelemetry _serverTelemetry;
        private readonly string _category;
        private readonly string _name;
        public readonly DateTime Start;
        public TelemetryTimer(IServerTelemetry serverTelemetry, string category, string name)
        {
            Start = DateTime.Now;
            _serverTelemetry = serverTelemetry;
            _category = category;
            _name = name;
        }

        public void Dispose()
        {
            var end = DateTime.Now;
            var span = end - Start;
            var miliseconds = Convert.ToInt32(span.TotalMilliseconds);
            _serverTelemetry.AddTimer(_category, _name, miliseconds);
        }
    }
}