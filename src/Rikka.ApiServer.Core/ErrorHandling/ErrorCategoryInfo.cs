namespace Rikka.ApiServer.Core.ErrorHandling
{
    public class ErrorCategoryInfo
    {
        public ErrorSubjectApplicationInfo Application => new ErrorSubjectApplicationInfo("A");
        public ErrorSubjectValidationInfo Validation => new ErrorSubjectValidationInfo("V");
        public ErrorSubjectUserInfo User => new ErrorSubjectUserInfo("P");
        public ErrorSubjectServerInfo Server => new ErrorSubjectServerInfo("S");
        public ErrorSubjectDataInfo Data => new ErrorSubjectDataInfo("D");
    }
}