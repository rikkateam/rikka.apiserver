namespace Rikka.ApiServer.Core.ErrorHandling
{
    public class ErrorSubjectValidationInfo
    {
        private readonly string _prefix;

        public ErrorSubjectValidationInfo(string prefix)
        {
            _prefix = prefix;
        }
        public ApiInternalException Get(ErrorSubjectValidation subject)
        {
            return new ApiInternalException(Error.GetCode(_prefix,(int)subject),Error.GetErrorMessage(subject));
        }
    }
}