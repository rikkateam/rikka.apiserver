namespace Rikka.ApiServer.Core.ErrorHandling
{
    public enum ErrorSubjectServer
    {
        ActionNotFound,
        VersionNotFound,
        SomeErrors,
        ImageNotFound,
        ActionWrongMethod
    }
}