namespace Rikka.ApiServer.Core.ErrorHandling
{
    public class ErrorSubjectDataInfo
    {
        private readonly string _prefix;

        public ErrorSubjectDataInfo(string prefix)
        {
            _prefix = prefix;
        }

        public ApiInternalException Get(ErrorSubjectData subject)
        {
            return new ApiInternalException(_prefix + ((int)subject).ToString("D5"), Error.GetErrorMessage(subject));
        }
    }
}