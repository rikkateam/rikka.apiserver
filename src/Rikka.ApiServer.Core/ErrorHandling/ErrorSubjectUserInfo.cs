namespace Rikka.ApiServer.Core.ErrorHandling
{
    public class ErrorSubjectUserInfo
    {
        private readonly string _prefix;

        public ErrorSubjectUserInfo(string prefix)
        {
            _prefix = prefix;
        }
        public ApiInternalException Get(ErrorSubjectUser subject)
        {
            return new ApiInternalException(Error.GetCode(_prefix, (int)subject), Error.GetErrorMessage(subject));
        }
    }
}