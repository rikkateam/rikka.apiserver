namespace Rikka.ApiServer.Core.ErrorHandling
{
    public class ErrorSubjectServerInfo
    {
        private readonly string _prefix;

        public ErrorSubjectServerInfo(string prefix)
        {
            _prefix = prefix;
        }
        public ApiInternalException Get(ErrorSubjectServer subject)
        {
            return new ApiInternalException(Error.GetCode(_prefix, (int)subject), Error.GetErrorMessage(subject));
        }
    }
}