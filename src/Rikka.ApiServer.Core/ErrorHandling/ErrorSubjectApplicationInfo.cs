namespace Rikka.ApiServer.Core.ErrorHandling
{
    public class ErrorSubjectApplicationInfo
    {
        private readonly string _prefix;

        public ErrorSubjectApplicationInfo(string prefix)
        {
            _prefix = prefix;
        }
        public ApiInternalException Get(ErrorSubjectApplication subject)
        {
            return new ApiInternalException(Error.GetCode(_prefix, (int)subject), Error.GetErrorMessage(subject));
        }
    }
}