namespace Rikka.ApiServer.Core.ErrorHandling
{
    public enum ErrorSubjectUser
    {
        LockedOut,
        WrongRequest
    }
}