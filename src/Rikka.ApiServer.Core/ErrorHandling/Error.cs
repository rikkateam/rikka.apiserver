using System;
using System.Linq.Expressions;

namespace Rikka.ApiServer.Core.ErrorHandling
{
    public static class Error
    {
        public static T Category<T>(Expression<Func<ErrorCategoryInfo, T>> expression)
        {
            return expression.Compile().Invoke(new ErrorCategoryInfo());
        }
        
        public static string GetCode(string prefix, int subcode)
        {
            return prefix + subcode.ToString("D5");
        }

        public static string GetErrorMessage(ErrorSubjectApplication subject)
        {
            //TODO:
            return $"Error {subject} of {subject.GetType().Name}";
        }

        public static string GetErrorMessage(ErrorSubjectValidation subject)
        {
            //TODO:
            return $"Error {subject} of {subject.GetType().Name}";
        }
        public static string GetErrorMessage(ErrorSubjectUser subject)
        {
            //TODO:
            return $"Error {subject} of {subject.GetType().Name}";
        }
        public static string GetErrorMessage(ErrorSubjectServer subject)
        {
            //TODO:
            return $"Error {subject} of {subject.GetType().Name}";
        }
        public static string GetErrorMessage(ErrorSubjectData subject)
        {
            //TODO:
            return $"Error {subject} of {subject.GetType().Name}";
        }
    }
}