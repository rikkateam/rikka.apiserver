using System;
using Rikka.ApiServer.Core.ErrorHandling;

namespace Rikka.ApiServer.Core
{
    public class ApiVersion
    {
        public override string ToString()
        {
            return $"{Major}.{Minor}.{Maintenance}";
        }

        public static ApiVersion FromString(string version)
        {
            var numbrs = version.Split(new[] { '.' }, StringSplitOptions.None);
            int major = 0, minor = 0, maintenance = 0;
            var cond = numbrs.Length == 3 && int.TryParse(numbrs[0], out major) &&
                       int.TryParse(numbrs[1], out minor) &&
                       int.TryParse(numbrs[2], out maintenance);
            if (!cond)
                throw Error.Category(c => c.Server).Get(ErrorSubjectServer.VersionNotFound);
            return new ApiVersion(major, minor, maintenance);
        }
        public ApiVersion(int major, int minor, int maintenance)
        {
            Major = major;
            Minor = minor;
            Maintenance = maintenance;
        }
        public readonly int Major;
        public readonly int Minor;
        public readonly int Maintenance;

        public override bool Equals(object obj)
        {
            var another = obj as ApiVersion;
            return Equals(another);
        }

        public bool Equals(ApiVersion obj)
        {
            return this == obj;
        }

        public override int GetHashCode()
        {
            //0.00.000
            return Major * 100000 + Minor * 1000 + Maintenance;
        }


        public static bool operator ==(ApiVersion left, ApiVersion right)
        {
            return left.Major == right.Major && left.Minor == right.Minor && left.Maintenance == right.Maintenance;
        }

        public static bool operator !=(ApiVersion left, ApiVersion right)
        {
            return left.Major != right.Major || left.Minor != right.Minor || left.Maintenance != right.Maintenance;
        }

        public static bool operator >(ApiVersion left, ApiVersion right)
        {
            return left.Major > right.Major
                   || (left.Major == right.Major && left.Minor > right.Minor)
                   || left.Major == right.Major && left.Minor == right.Minor && left.Maintenance > right.Maintenance;
        }

        public static bool operator <(ApiVersion left, ApiVersion right)
        {
            return left.Major < right.Major
                   || (left.Major == right.Major && left.Minor < right.Minor)
                   || left.Major == right.Major && left.Minor == right.Minor && left.Maintenance < right.Maintenance;
        }

        public static bool operator >=(ApiVersion left, ApiVersion right)
        {
            return left.Major >= right.Major
                   || (left.Major == right.Major && left.Minor >= right.Minor)
                   || left.Major == right.Major && left.Minor == right.Minor && left.Maintenance >= right.Maintenance;
        }

        public static bool operator <=(ApiVersion left, ApiVersion right)
        {
            return left.Major <= right.Major
                   || (left.Major == right.Major && left.Minor <= right.Minor)
                   || left.Major == right.Major && left.Minor == right.Minor && left.Maintenance <= right.Maintenance;
        }
    }
}