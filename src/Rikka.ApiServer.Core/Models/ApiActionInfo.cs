using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Microsoft.Extensions.Primitives;

namespace Rikka.ApiServer.Core.Models
{
    [Flags]
    public enum HttpMethods
    {
        None=0,
        Get=1,
        Post=2,
        Put=4,
        Delete=8
    }
    public interface IActionInfo
    {
        HttpMethods HttpMethods { get; set; }
        string Action { get; set; }
        IDictionary<string, string> Parameters { get; set; }
        IDictionary<string, string> HttpHeaders { get; set; }
        IDictionary<string, StringValues> QueryParams { get; set; }
    }
    public class ActionInfo : IActionInfo
    {
        public ActionInfo()
        {
            Parameters = new Dictionary<string, string>();
            HttpHeaders = new Dictionary<string, string>();
            QueryParams = new Dictionary<string, StringValues>();
        }

        public ActionInfo(string action, IDictionary<string, string> httpHeaders, IDictionary<string, StringValues> queryParams)
        {
            Action = action;
            Parameters = new Dictionary<string, string>();
            HttpHeaders = httpHeaders;
            QueryParams = queryParams;
        }

        public string Action { get; set; }
        public string Version { get; set; }
        public string DeviceId { get; set; }
        public string Platform { get; set; }
        public string PlatformVersion { get; set; }
        public string SecureKey { get; set; }
        public string Timezone { get; set; }
        public int TimezoneInt => GetTimezoneInt();
        public string Locale { get; set; }
        public int GetTimezoneInt()
        {
            int result;
            return int.TryParse(Timezone, out result) ? result : 0;
        }

        public IDictionary<string, string> Parameters { get; set; }
        public HttpMethods HttpMethods { get; set; }
        public IDictionary<string, string> HttpHeaders { get; set; }
        public IDictionary<string, StringValues> QueryParams { get; set; }
    }
}