﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Rikka.ApiServer.Core
{
    public interface ISerializer
    {
        object Deserialize(Stream stream, Type type);
        void Serialize(Stream stream, object model);
    }
}
