﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Rikka.ApiServer.Core.Attributes;
using Rikka.ApiServer.Hosting;

namespace Rikka.ApiServer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApiServer(options =>
            {
                options.Applications.Add(new IsolatedApplicationMetaData("/api/1.0.0/test-api", typeof(TestApi), typeof(ITestApi)));
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseApiServer();
        }
    }


    public interface ITestApi
    {
        decimal Sum(SumMessage message);
        decimal Sub(ISubMessage message);
    }

    [ApiContextMark("1.0.0")]
    [ApiTypeMap(typeof(ISubMessage), typeof(SubMessage))]
    public class TestApi : ITestApi
    {
        public decimal Sum(SumMessage message)
        {
            return message.Left + message.Right;
        }

        public decimal Sub(ISubMessage message)
        {
            return message.Left - message.Right;
        }
    }

    public class SumMessage
    {
        public decimal Left { get; set; }
        public decimal Right { get; set; }
    }

    public interface ISubMessage
    {
        decimal Left { get; set; }
        decimal Right { get; set; }
    }

    public class SubMessage : ISubMessage
    {
        public decimal Left { get; set; }
        public decimal Right { get; set; }
    }
}
