using System;

namespace Rikka.ApiServer.Builder.Attributes
{
    
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ApiRequestMapAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">��� ����������� <see cref="IApiRequestMapper"/></param>
        public ApiRequestMapAttribute(Type type)
        {
            Type = type;
        }
        public Type Type { get; set; }
    }
}