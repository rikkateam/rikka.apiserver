using System;
using Rikka.ApiServer.Core;

namespace Rikka.ApiServer.Builder.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ApiSerializerAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">�� ����������� <see cref="ISerializer"/></param>
        /// <param name="contentType"></param>
        public ApiSerializerAttribute(Type type, params string[] contentType)
        {
            Type = type;
            ContentType = contentType;
        }

        public string[] ContentType;
        public Type Type { get; set; }
    }
}