﻿using Microsoft.AspNetCore.Http;
using Rikka.ApiServer.Core.Models;

namespace Rikka.ApiServer.Builder
{
    public interface IApiRequestMapper
    {
        IActionInfo Check(HttpRequest request);
    }
}
