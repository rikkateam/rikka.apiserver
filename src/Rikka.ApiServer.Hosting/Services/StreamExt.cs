﻿using System;
using System.IO;
using System.Text;

namespace Rikka.ApiServer.Hosting.Services
{
    public static class StreamExt
    {
        public static string ReadString(this Stream stream)
        {
            var buffer = new ArraySegment<byte>();
            using (var memory = new MemoryStream())
            {
                stream.CopyTo(memory);
                memory.TryGetBuffer(out buffer);
            }
            return Encoding.UTF8.GetString(buffer.Array);
        }
    }
 
}