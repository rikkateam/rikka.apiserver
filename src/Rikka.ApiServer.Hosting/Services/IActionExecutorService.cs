using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Rikka.ApiServer.Core;
using Rikka.ApiServer.Core.Models;

namespace Rikka.ApiServer.Hosting.Services
{
    public interface IActionExecutorService
    {
        void Load(IIsolatedApplicationMetaData metaData);
        Task<object> Exec(IActionInfo info, Type contexType, Stream requestStream);
        IActionInfo GetSelector(HttpRequest request, Type context);
    }
}