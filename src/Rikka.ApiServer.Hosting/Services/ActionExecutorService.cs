using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Rikka.ApiServer.Core;
using Rikka.ApiServer.Core.Attributes;
using Rikka.ApiServer.Core.ErrorHandling;
using Rikka.ApiServer.Core.Models;
using Rikka.ApiServer.Core.Telementry;
using Rikka.ApiServer.Hosting.System;
using Rikka.ApiServer.Hosting.Telemetry;

namespace Rikka.ApiServer.Hosting.Services
{
    public class ActionExecutorService : IActionExecutorService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IServerTelemetry _serverTelemetry;
        private readonly Dictionary<Type,ContextExecutor> _executors;

        public ActionExecutorService(IServiceProvider serviceProvider, IServerTelemetry serverTelemetry)
        {
            _serviceProvider = serviceProvider;
            _serverTelemetry = serverTelemetry;
            _executors = new Dictionary<Type, ContextExecutor>();
        }            

        public async Task<object> Exec(IActionInfo info, Type contexType, Stream requestStream, string contentType)
        {
            var executor = _executors[contexType];
            if (!executor.Actions.ContainsKey(info.Action))
                throw Error.Category(c => c.Server).Get(ErrorSubjectServer.ActionNotFound);
            if(!executor.Actions[info.Action].HttpMethods.HasFlag(info.HttpMethods))
                throw Error.Category(c => c.Server).Get(ErrorSubjectServer.ActionWrongMethod);
            object result;
            using (new TelemetryTimer(_serverTelemetry, "ActionExecution", info.Action))
            {
                var type = executor.ParameterType(info.Action);
                var serizalier = executor.GetSerializer(contentType);
                var message = serizalier.Deserialize(requestStream, type);
                result = await executor.Exec(requestStream, info);
            }
            return result;
        }

        public void Load(IIsolatedApplicationMetaData metaData)
        {
            var mark = metaData.ContextType.GetTypeInfo().GetCustomAttribute<ApiContextMarkAttribute>();
            if (mark == null)
                throw Error.Category(c => c.Server).Get(ErrorSubjectServer.SomeErrors);
            var executor = new ContextExecutor(mark.Version, metaData, _serviceProvider);
            _executors.Add(metaData.ContextType, executor);
        }

        public IActionInfo GetSelector(HttpRequest request, Type context)
        {
            var executor = _executors.Values.FirstOrDefault(f => f.ContextType == context);
            return executor?.RequestMapper.Check(request);
        }
    }  
}