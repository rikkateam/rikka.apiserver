using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Rikka.ApiServer.Builder;
using Rikka.ApiServer.Core.Models;

namespace Rikka.ApiServer.Hosting.System
{
    public class SimpleRequestMapper : IApiRequestMapper
    {        

        public virtual IActionInfo Check(HttpRequest request)
        {
            if (!request.Path.StartsWithSegments(_metaData.Path))
                return null;
            var pathElement = request.Path.Value.Substring(_metaData.Path.Length);
            //TODO: ������������� ��������� ��� ������� ������ �� ����
            var executorActionInfo = _actions.Values.FirstOrDefault(f => f.PathTemplate == pathElement);
            if (executorActionInfo == null)
                return null;
            return GetActionInfo(executorActionInfo, request);
        }

        protected IActionInfo GetActionInfo(ExecutorActionInfo executorActionInfo, HttpRequest request)
        {
            var headers = new ConcurrentDictionary<string, string>();
            foreach (var header in request.Headers)
            {
                headers.AddOrUpdate(header.Key, header.Value, (a, b) => header.Value);
            }
            var queryParams = request.Query.ToDictionary(query => query.Key, query => query.Value);
            var info = new ActionInfo(executorActionInfo.Name, headers, queryParams);
            switch (request.Method)
            {
                case "GET":
                    info.HttpMethods = HttpMethods.Get;
                    break;
                case "POST":
                    info.HttpMethods = HttpMethods.Post;
                    break;
                case "PUT":
                    info.HttpMethods = HttpMethods.Put;
                    break;
                case "DELETE":
                    info.HttpMethods = HttpMethods.Delete;
                    break;
            }
            return info;
        }
    }
}