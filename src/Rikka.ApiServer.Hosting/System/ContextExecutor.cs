using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Rikka.ApiServer.Builder;
using Rikka.ApiServer.Builder.Attributes;
using Rikka.ApiServer.Core;
using Rikka.ApiServer.Core.Attributes;
using Rikka.ApiServer.Core.ErrorHandling;
using Rikka.ApiServer.Core.Models;
using Rikka.ApiServer.Core.Telementry;
using Rikka.ApiServer.Hosting.Services;

namespace Rikka.ApiServer.Hosting.System
{
    public class ContextExecutor
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IServerTelemetry _serverTelemetry;

        public readonly ApiVersion Version;

        public readonly Type ContractType;
        public readonly Type ContextType;

        public readonly IReadOnlyDictionary<string, ExecutorActionInfo> Actions;
        public readonly IReadOnlyDictionary<Type, Type> TypeMap;
        public readonly IApiRequestMapper RequestMapper;
        public readonly IDictionary<string,ISerializer> Serializers;

        public ContextExecutor(ApiVersion version, IIsolatedApplicationMetaData metaData, IServiceProvider serviceProvider)
        {
            Version = version;
            ContractType = metaData.ContractType;
            ContextType = metaData.ContextType;
            _serviceProvider = serviceProvider;
            var typeMaps = metaData.ContextType.GetTypeInfo().GetCustomAttributes<ApiTypeMapAttribute>();
            TypeMap = typeMaps.ToDictionary(s => s.FromType, s => s.ToType);
            Actions = GetActions().ToDictionary(s => s.Name, s => s);
            _serverTelemetry = serviceProvider.GetService(typeof(IServerTelemetry)) as IServerTelemetry;
            var requestMap = metaData.ContextType.GetTypeInfo().GetCustomAttribute<ApiRequestMapAttribute>();
            var requestMapType = requestMap?.Type ?? typeof(SimpleRequestMapper);
            RequestMapper = serviceProvider.GetService(requestMapType) as IApiRequestMapper;
            Serializers = new Dictionary<string, ISerializer>();
            var serializers = metaData.ContextType.GetTypeInfo().GetCustomAttributes<ApiSerializerAttribute>();
            foreach (var serializer in serializers)
            {
                foreach (var type in serializer.ContentType)
                {
                    //TODO: ��������
                    if(!Serializers.ContainsKey(type))
                        Serializers.Add(type, serviceProvider.GetService(serializer.Type) as ISerializer);
                }
            }
        }

        private Type MapType(Type type)
        {
            return TypeMap.ContainsKey(type) ? TypeMap[type] : type;
        }

        public Type ParameterType(string action) => MapType(Actions[action].ParamType);

        public async Task<object> Exec(object message, IActionInfo info)
        {
            var action = Actions[info.Action];
            object context;
            using (new TelemetryTimer(_serverTelemetry, "ContextExecutor", "ContextCreating"))
            {
                context = GetContext(info);
            }
            object result;
            using (new TelemetryTimer(_serverTelemetry, "ContextExecutor", "Executing"))
            {
                result = await _invokeAsync(action.MethodInfo, context, message);
            }
            return result;
        }

        #region invoking
        private Task<object> _invokeAsync(MethodBase methodInfo, object context, object message)
        {
            return Task.Run(() => _invokeSync(methodInfo, context, message));
        }
        private object _invokeSync(MethodBase methodInfo, object context, object message)
        {
            return methodInfo.Invoke(context, new[] { message });
        }
        #endregion        

        #region loading
        private ExecutorActionInfo GetAction(ApiVersion version, MethodBase methodInfo)
        {
            var parameters = methodInfo.GetParameters();
            if (parameters.Length != 1)
                throw Error.Category(c => c.User).Get(ErrorSubjectUser.WrongRequest);
            var param = parameters[0];
            var pathTemplate = methodInfo.GetCustomAttribute<ApiPathTemplateAttribute>();
            var anonymous = methodInfo.GetCustomAttribute<ApiAnonymousAttribute>();
            var httpGet = methodInfo.GetCustomAttribute<ApiHttpGetAttribute>();
            var httpPost = methodInfo.GetCustomAttribute<ApiHttpPostAttribute>();
            var httpPut = methodInfo.GetCustomAttribute<ApiHttpPutAttribute>();
            var httpDelete = methodInfo.GetCustomAttribute<ApiHttpDeleteAttribute>();
            var methods = HttpMethods.None;
            if (httpGet != null)
                methods = methods | HttpMethods.Get;
            if (httpPost != null)
                methods = methods | HttpMethods.Post;
            if (httpPut != null)
                methods = methods | HttpMethods.Put;
            if (httpDelete != null)
                methods = methods | HttpMethods.Delete;
            var info = new ExecutorActionInfo(methodInfo.Name, version, param.ParameterType, methodInfo, methods, pathTemplate?.Template)
            {
                CastGeneric = methodInfo.IsGenericMethod,
                Anonymous = anonymous != null
            };
            return info;
        }

        private IEnumerable<ExecutorActionInfo> GetActions()
        {
            var methods = ContractType.GetTypeInfo().GetMethods();
            return methods.Select(method => GetAction(Version, method));
        }


        private object GetContext(IActionInfo info)
        {
            var constructor = ContextType.GetConstructors()[0];
            var args = new List<object>();
            foreach (var param in constructor.GetParameters())
            {
                if (param.ParameterType == typeof(IActionInfo))
                    args.Add(info);
                else
                    args.Add(_serviceProvider.GetService(param.ParameterType));
            }
            var instance = Activator.CreateInstance(ContextType, args.ToArray());
            return instance;
        }
        #endregion

        public ISerializer GetSerializer(string contentType)
        {
            throw new NotImplementedException();
        }
    }
}