using System;
using System.Reflection;
using Rikka.ApiServer.Core;
using Rikka.ApiServer.Core.Models;

namespace Rikka.ApiServer.Hosting.System
{
    public class ExecutorActionInfo
    {
        public ExecutorActionInfo(string name, ApiVersion version, Type paramType, MethodBase methodInfo,HttpMethods httpMethods,string pathTemplate)
        {
            Name = name;
            Version = version;
            ParamType = paramType;
            MethodInfo = methodInfo;
            HttpMethods = httpMethods;
            PathTemplate = pathTemplate;
        }

        public string Name { get; }
        public string PathTemplate { get; }
        public ApiVersion Version { get; }
        public Type ParamType { get; }
        public HttpMethods HttpMethods { get; }
        public MethodBase MethodInfo { get; }

        public bool Anonymous { get; set; }
        public bool CastGeneric { get; set; }
    }
}