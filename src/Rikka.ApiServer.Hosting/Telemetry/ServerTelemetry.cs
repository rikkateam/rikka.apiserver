﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rikka.ApiServer.Core.Telementry;

namespace Rikka.ApiServer.Hosting.Telemetry
{
    public class ServerTelemetry:IServerTelemetry
    {
        public ServerTelemetry()
        {
            Collection = new List<TelemetryInfo>();
        }
        public void AddTimer(string category, string name, int miliseconds)
        {            
            var item = new TelemetryInfo("timer",category,name,miliseconds.ToString());
            Collection.Add(item);
        }

        protected readonly List<TelemetryInfo> Collection;

        public async Task Store()
        {
            var items = Collection.ToArray();
            Collection.Clear();
            //TODO: сделать сохранение в монго
            throw new NotImplementedException();
            
        }

        public void AddHit(string category, string name)
        {
            throw new NotImplementedException();
        }
    }
}
