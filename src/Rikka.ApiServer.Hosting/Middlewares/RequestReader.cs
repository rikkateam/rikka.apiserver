﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Rikka.ApiServer.Hosting.Services;

namespace Rikka.ApiServer.Hosting.Middlewares
{
    public class RequestReader
    {
        private readonly RequestDelegate _next;
        private readonly IActionExecutorService _actionExecutorService;
        private readonly ApiServerOptions _options;

        public RequestReader(RequestDelegate next, IActionExecutorService actionExecutorService, ApiServerOptions options)
        {
            _next = next;
            _actionExecutorService = actionExecutorService;
            _options = options;
        }

        public async Task Invoke(HttpContext context)
        {
            var stream = context.Request.Body;
            if (stream == Stream.Null || stream.CanSeek)
            {
                await _next(context);
                return;
            }
            try
            {
                using (var buffer = new MemoryStream())
                {
                    await stream.CopyToAsync(buffer);
                    buffer.Position = 0L;
                    context.Request.Body = buffer;
                    //                    
                    var metaData = _options.Applications.FirstOrDefault(app => context.Request.Path.StartsWithSegments(app.Path));
                    if (metaData != null)
                    {
                        var selector = _actionExecutorService.GetSelector(context.Request,metaData.ContextType);
                        if (selector == null)
                        {
                            throw new NotImplementedException("отобразить ошибку");
                        }
                        var result = await _actionExecutorService.Exec(selector, metaData.ContextType, context.Request.Body,context.Request.ContentType);
                        throw new NotImplementedException("вернуть результат");
                    }
                    //
                    await _next(context);
                }
            }
            finally
            {
                //context.Request.Body = stream;
            }
        }
    }
}
