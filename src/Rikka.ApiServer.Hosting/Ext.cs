﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Rikka.ApiServer.Core.Telementry;
using Rikka.ApiServer.Hosting.Middlewares;
using Rikka.ApiServer.Hosting.Services;
using Rikka.ApiServer.Hosting.Telemetry;

namespace Rikka.ApiServer.Hosting
{
    public static class ApiServerBuilderExt
    {
        public static IApplicationBuilder UseApiServer(this IApplicationBuilder builder)
        {
            var options = builder.ApplicationServices.GetService<ApiServerOptions>();
            var actionExecutorService = builder.ApplicationServices.GetService<IActionExecutorService>();
            foreach (var application in options.Applications)
            {
                builder.Map(application.Path, inner =>
                {
                    inner.UseMiddleware<RequestReader>();
                    inner.UseMiddleware<RequestCultureMiddleware>();
                });
                actionExecutorService.Load(application);
            }
            return builder;
        }
        

        public static IServiceCollection AddApiServer(this IServiceCollection services, ApiServerOptions options)
        {
            services.AddSingleton(options);
            services.AddTransient<IServerTelemetry, ServerTelemetry>();
            services.AddTransient<IActionExecutorService, ActionExecutorService>();
            return services;
        }
        public static IServiceCollection AddApiServer(this IServiceCollection services, Action<ApiServerOptions> func)
        {
            var options = new ApiServerOptions();
            func.Invoke(options);
            return services.AddApiServer(options);
        }
    }

    public class ApiServerOptions
    {
        public ApiServerOptions()
        {
            Applications = new List<IIsolatedApplicationMetaData>();
        }
        public IList<IIsolatedApplicationMetaData> Applications { get; set; }
    }

    public interface IIsolatedApplicationMetaData
    {
        string Path { get; }
        Type ContextType { get; }
        Type ContractType { get; }
    }
    public class IsolatedApplicationMetaData: IIsolatedApplicationMetaData
    {
        public IsolatedApplicationMetaData(string path, Type contextType, Type contractType)
        {
            Path = path;
            ContextType = contextType;
            ContractType = contractType;
        }

        public string Path { get; set; }
        public Type ContextType { get; set; }
        public Type ContractType { get; set; }       
    }

}
